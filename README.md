# Grid-solver #

This application's primary functionality is to provide the output of GRID-cell by matching the entered string with GRID-PDF file.


## Prerequisites ##
	
* Installation of node
	* Download from [here](https://nodejs.org/en/download/) and install it

### How do I get set up? ###

* npm install
	* Installs all dependency present in package.json
	
* npm start
	* To start the application
	* To check if the app is running, browse to: .[localhost:3333]. 'http://localhost:3333/'


### Who do I talk to? ###

* Sachin Saini