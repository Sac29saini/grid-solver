/* All Global JS is present here..! */

/* Change your credential here */
var config = {
    "PASSWORD": 'yourVDIPassword',
    'pdfFileName': 'grid' // Should be without Extension
}

var customGridObj = {
        'A': [],
        'B': [],
        'C': [],
        'D': [],
        'E': [],
        'F': [],
        'G': [],
        'H': [],
        'I': [],
        'J': [],
        'K': [],
        'L': []
    },
    savedPwd,
    eventBindObj = [
        {
            selector: '#calculateBtn',
            handler: computeGridCellValue
        },
        {
            event: 'keyup',
            selector: '#inputGrid',
            handler: triggerFind
        },
        {
            selector: '#resetBtn',
            handler: resetGridString
        },
        {
            selector: '#copyPwd',
            handler: copyPassword
        },
        {
            selector: '#copyOutput',
            handler: copyGridOutput
        },
        {
            event: 'focus',
            selector: '#inputGrid',
            handler: resetGridString
        },
        {
            selector: '.view-image',
            handler: showPassword
        },
        {
            selector: '#setPwdLocal',
            handler: updatePwd
        },
        {
            selector: '#setChangePwd',
            handler: toggleInputPwdBlock
        },
        {
            selector: '.guide-btn',
            handler: toggleUserGuide
        },
        {
            selector: '.view-video',
            handler: displayRefVideo
        },
        {
            selector: '.back-from-video',
            handler: displayRefVideo
        }
    ]
$(document).ready(function() {
    console.log("Grid ready!");
    
    eventBind(eventBindObj);
    pdfCalculate();
    setPwdFromlocal();
});

function eventBind(eventObj) {
    for(var obj of eventObj) {
        $(obj.selector).on((obj.event) ? obj.event : 'click', obj.handler);
    }
}

function toggleUserGuide() {
    $(this).find('.ham-main').toggle();
    $(this).find('.ham-cross').toggle();
    $('.guide-content').toggle('slow');
}

function displayRefVideo() {
    $('.step-block').toggle('slow');
    $('.video-block').toggle();
    $('.back-from-video').toggle($('.video-block').is(':visible'));
}
/**
 * @function showPassword: Display the password and Enable the edit/update mode
 */
function showPassword() {
    var valueToBeSet = $('#setPwdInput').prop('disabled') ? savedPwd : '********'; // '*'.repeat(savedPwd.length) - Not supported in IE
    $('#setPwdInput').val(valueToBeSet);
    $('#setPwdLocal').prop('disabled', !$('#setPwdLocal').prop('disabled'));
    $('#setPwdInput').prop('disabled', !$('#setPwdInput').prop('disabled'));
}

/**
 * @function toggleInputPwdBlock: Toggle the Password edit block
 */
function toggleInputPwdBlock() {
    $(this).closest('.input-group-prepend').siblings().toggle('slow');
}

/**
 * @function updatePwd: Set/Update the password into the Local-Storage 
 */
function updatePwd() {
    var currentInputVal = $('#setPwdInput').val();
    if(currentInputVal.length > 6) {
        window.localStorage.setItem('savedPwd', currentInputVal);
        setPwdFromlocal();
        copyPassword();
    }
    else {
        alert('Please check your entered password length')
    }
}

/**
 * @function setPwdFromlocal: Read the password from LocalStorage and saved into the localVariable.
 */
function setPwdFromlocal() {
    savedPwd = window.localStorage.getItem('savedPwd');
    if(savedPwd) {
        // var tempPwd = '*'.repeat(savedPwd.length); // Not supported in IE
        $('#setPwdInput').val('********').data('val', savedPwd).prop('disabled', true);
        $('#setPwdLocal').html('Update').prop('disabled', true);
        $('.set-text').hide();
        $('.update-text').show();
        $('.view-image').show();
    }        
}

/**
 * @function triggerFind: Compute the Grid value on pressing Enter key
 */
function triggerFind(event) {
    if(event.keyCode == 13) {
        $('#calculateBtn').click();
    }
}

/**
 * @function resetGridString: Reset the text of Grid-Input cell
 */
function resetGridString() {
    $('#inputGrid').val('');
}

/**
 * @function computeGridCellValue: Extract the necessary code from enter string and find their respective value from the Stored Local String Object.
 */
function computeGridCellValue() {
    var givenStr = $('#inputGrid').val();
    if (givenStr.includes('[')) {
        var splitArr = givenStr.split('[');
        var newArr = [];
        newArr[0] = splitArr[1].substring(0, 2);
        newArr[1] = splitArr[2].substring(0, 2);
        newArr[2] = splitArr[3].substring(0, 2);


        var a1 = newArr[0][0].toUpperCase();
        var a2 = newArr[0][1];

        var b1 = newArr[1][0].toUpperCase();
        var b2 = newArr[1][1];

        var c1 = newArr[2][0].toUpperCase();
        var c2 = newArr[2][1];

        var o1 = customGridObj[a1][a2 - 1];
        var o2 = customGridObj[b1][b2 - 1];
        var o3 = customGridObj[c1][c2 - 1];

        var output = o1 + o2 + o3;
        $('#gridOutput').val(output);
    } else {
        console.log('Please check the value entered, it doesn\'t look like what we need to calculate!');
        alert("Please Check the Entered value..!");
    }

}

/**
 * @function copyGridOutput: Copy the Grid Output into the Clipboard
 */
function copyGridOutput() {
    $("#gridOutput").select();
    document.execCommand("Copy");
}

/**
 * @function copyPassword: Copy the stored password into the clipboard
 */
function copyPassword() {
    var myPass = config.PASSWORD;
    var reqBlock = $('#pastedPwd');
    reqBlock.val((savedPwd)? savedPwd : myPass);
    reqBlock.removeClass('is-hidden');
    reqBlock.select();
    document.execCommand("Copy");
    reqBlock.addClass('is-hidden');
}

/**
 * @function pdfCalculate: Read the given PDF file, and convert whole PDF into a String Object and then 
 * made a gridMatrix from this String Object
 */
function pdfCalculate() {
    var url = config.pdfFileName + '.pdf';
    PDFJS.workerSrc = './vendor/pdf.worker.js';

    // Asynchronous download of PDF
    var loadingTask = PDFJS.getDocument(url);
    loadingTask.promise.then(function(pdf) {
        console.log('PDF loaded');

        var pageNumber = 1,
            str = [];

        pdf.getPage(1).then(function(page) {
                page.getTextContent().then(function(textContent) {
                    var initial = (textContent.items[13].str == '') ? 14 : 13,
                        final = initial + 98;
                    for (var j = initial+1; j < final; j++) {
                        customGridObj['A'].push(textContent.items[j++].str); //15
                        customGridObj['B'].push(textContent.items[j++].str);
                        customGridObj['C'].push(textContent.items[j++].str);
                        customGridObj['D'].push(textContent.items[j++].str);
                        customGridObj['E'].push(textContent.items[j++].str);
                        customGridObj['F'].push(textContent.items[j++].str);
                        customGridObj['G'].push(textContent.items[j++].str);
                        customGridObj['H'].push(textContent.items[j++].str);
                        customGridObj['I'].push(textContent.items[j++].str);
                        customGridObj['J'].push(textContent.items[j++].str);
                        customGridObj['K'].push(textContent.items[j++].str);
                        customGridObj['L'].push(textContent.items[j++].str); //26
                        str.push(textContent.items[j].str);
                        j++;
                    }
                });
            })
    }, function(reason) {
        // PDF loading error
        console.error(reason);
    });
}