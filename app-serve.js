const express = require('express');
const app = express();
const path = require('path');

// const __dirname = 'C:/Users/ssain851/Desktop/';
app.use(express.static(path.join(__dirname, '/public')));

app.get('/', (req, res) => res.sendFile(__dirname + '/index.html'))

app.listen(3333, () => console.log('\x1b[32m%s\x1b[0m', '\tApp is listening on port 3333! \n\n\tPlease hit on \'http://localhost:3333/\'\n'));